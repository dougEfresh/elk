module gitlab.com/dougEfresh/elk/v7

go 1.14

require (
	github.com/hashicorp/hcl v1.0.0
	github.com/olebedev/when v0.0.0-20190311101825-c3b538a97254
	github.com/olivere/elastic/v7 v7.0.17
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20190426145343-a29dc8fdc734 // indirect
	golang.org/x/sys v0.0.0-20190502175342-a43fa875dd82 // indirect
)
