/*
Copyright © 2020 Douglas Chimento <dchimento@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/dougEfresh/elk/v7/pkg/config"
	"gitlab.com/dougEfresh/elk/v7/pkg/eval"
	"gitlab.com/dougEfresh/elk/v7/pkg/log"
	"gitlab.com/dougEfresh/elk/v7/pkg/query"
)

func execSearch(esConfig *config.EsConfig, queryDef *config.QueryDefinition, format *config.Format) error {
	log.Logger.Debugf("\nEsConfig: %s\nQuery: %s", esConfig, queryDef)
	if queryDef.Index == "" {
		return fmt.Errorf("invalid Query: %s\nNo index defined", queryDef)
	}
	q, err := query.New(esConfig)
	if err != nil {
		return err
	}
	hits := make(chan *query.Hit)
	if err = q.Search(context.Background(), queryDef, hits); err != nil {
		return err
	}
	return eval.Write(hits, os.Stdout, format)
}
