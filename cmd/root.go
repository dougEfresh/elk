/*
Copyright © 2020 Douglas Chimento <dchimento@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/dougEfresh/elk/v7/pkg/config"
	"gitlab.com/dougEfresh/elk/v7/pkg/log"
)

// nolint:lll
func NewRootCommand() *cobra.Command {
	rootCmd := &cobra.Command{
		Use:  "elk",
		Long: `https://gitlab.com/dougEfresh/elk - cat,grep indexes in elastic search`,
	}
	defaultConfig := mergeLocalConfig(config.DefaultConfig())
	cliConfig := &config.Config{
		EsConfig:        &config.EsConfig{},
		QueryDefinition: &config.QueryDefinition{},
		Format:          &config.Format{},
		Verbose:         false,
	}
	rootCmd.PersistentFlags().StringVarP(&cliConfig.EsConfig.URL, "url", "u", "", "url of es")
	rootCmd.PersistentFlags().StringArrayVarP(&cliConfig.Format.Fields, "fields", "f", []string{}, "output format")
	rootCmd.PersistentFlags().StringVarP(&cliConfig.QueryDefinition.Index, "index", "i", "", "index(s) to search")
	rootCmd.PersistentFlags().StringVarP(&cliConfig.QueryDefinition.TimestampField, "timestamp", "t", "", "timestamp field")
	rootCmd.PersistentFlags().StringVar(&cliConfig.QueryDefinition.FromHCL, "from", "", "look for records after this time")
	rootCmd.PersistentFlags().BoolVarP(&cliConfig.Verbose, "verbose", "v", false, "verbose logs")
	rootCmd.PersistentFlags().StringVar(&cliConfig.EsConfig.Tracelog, "trace-file", "", "store ES trace logs in this location")
	rootCmd.PersistentFlags().StringVar(&cliConfig.EsConfig.Infolog, "info-file", "", "store ES info logs in this location")
	rootCmd.PersistentFlags().StringVar(&cliConfig.EsConfig.Errorlog, "error-file", "", "store ES error logs in this location")
	rootCmd.PersistentFlags().BoolVar(&cliConfig.DryRun, "dry-run", false, "show query/elasticsearch config but don't execute anything")
	rootCmd.RunE = execMergeConfig(cliConfig, defaultConfig.EsConfig, defaultConfig.QueryDefinition, defaultConfig.Format)
	rootCmd.PersistentPreRun = func(cmd *cobra.Command, args []string) {
		if cliConfig.Verbose {
			log.Logger.SetLevel(logrus.DebugLevel)
		}
	}
	loadCommands(rootCmd, defaultConfig, cliConfig)
	rootCmd.AddCommand(newConfCmd())
	return rootCmd
}

func mergeLocalConfig(defaultConfig *config.Config) *config.Config {
	var mergedConfig *config.Config = defaultConfig
	localCfg, err := config.LoadLocalConfig()
	if err != nil {
		log.Logger.Warnf("failed to parse config at %s %s", config.LocalConfigLocation, err)
	}
	if localCfg == nil {
		return mergedConfig
	}
	mergedConfig = defaultConfig.Merge(localCfg)
	return mergedConfig
}

func loadCommands(rootCmd *cobra.Command, cfg *config.Config, cliConfig *config.Config) {
	for _, command := range cfg.Commands {
		cmd := &cobra.Command{
			Use:   command.Name,
			Short: command.Description,
			RunE:  execMergeConfig(cliConfig, command.EsConfig, command.QueryDefinition, command.Format),
			Args:  cobra.NoArgs,
		}
		for _, subCommand := range command.SubCommands {
			subCmd := &cobra.Command{
				Use:   subCommand.Name,
				Short: subCommand.Description,
				RunE:  execMergeConfig(cliConfig, command.EsConfig, subCommand.QueryDefinition, subCommand.Format),
				Args:  cobra.NoArgs,
			}
			cmd.AddCommand(subCmd)
		}
		rootCmd.AddCommand(cmd)
	}
}

// nolint:lll
func execMergeConfig(cliConfig *config.Config,
	esConfig *config.EsConfig, queryDef *config.QueryDefinition,
	format *config.Format) func(_ *cobra.Command, _ []string) error {
	return func(_ *cobra.Command, _ []string) error {
		var err error
		mergedEsConfig := esConfig.Merge(cliConfig.EsConfig)
		mergedQuery := queryDef.Merge(cliConfig.QueryDefinition)
		mergedFormat := format.Merge(cliConfig.Format)

		if mergedQuery.TimestampField == "" {
			// make sure a default timestamp is set
			mergedQuery.TimestampField = "@timestamp"
		}
		queryDef.From, err = config.ParseFrom(queryDef.FromHCL)
		if err != nil {
			return err
		}
		if cliConfig.DryRun {
			_, err = fmt.Fprintf(os.Stdout, "DRY RUN\nEsConfig: %s\nQuery: %s\nFormat:%s\n", mergedEsConfig, mergedQuery, mergedFormat)
			return err
		}
		return execSearch(mergedEsConfig, mergedQuery, mergedFormat)
	}
}
