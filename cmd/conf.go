package cmd

import (
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/dougEfresh/elk/v7/pkg/config"
)

func newConfCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "conf",
		Short: "show default config or save default config to " + config.LocalConfigLocation,
	}
	var save = cmd.Flags().Bool("save", false, "write default config to "+config.LocalConfigLocation)
	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		var w io.Writer = os.Stdout
		if *save {
			fmt.Println("saving...")
			return nil
		}
		_, err := fmt.Fprintf(w, "%s\n", defConfig)
		return err
	}
	return cmd
}

const defConfig = `
// global elastic search connection config for all custom commands/profiles
// see ...
es {
  
}

// global query definition for all custom commands/profiles
query {

}

/*
// profiles can be used to ...
profile "yesterday" {
  query {
    since = "yesterday"
  }
}

command "my-webserver" {
   query {

   }
}
*/

`
