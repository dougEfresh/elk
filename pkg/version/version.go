package version

// All these values will be replaced by actual values at build time.
// These are globals because they are fed by ldflags at compile time.
var (
	GitHash          = "-"       // nolint:gochecknoglobals
	GitBranch        = "-"       // nolint:gochecknoglobals
	GitTag           = "-"       // nolint:gochecknoglobals
	GitCommitMessage = "-"       // nolint:gochecknoglobals
	BuildTime        = "-"       // nolint:gochecknoglobals
	ServiceName      = "dseeker" // nolint:gochecknoglobals
)
