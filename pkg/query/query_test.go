package query

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dougEfresh/elk/v7/pkg/config"
)

// nolint:lll
func TestConvert(t *testing.T) {
	since, err := config.ParseFrom("2020-04-06T12:55:00")
	require.NoError(t, err)
	def := &config.QueryDefinition{
		Index:          "index",
		Terms:          map[string]string{"level": "info"},
		TimestampField: "@timestamp",
		From:           since.UTC(),
	}
	q, err := convertQuery(def)
	require.NoError(t, err)
	src, err := q.Source()
	require.NoError(t, err)
	b, err := json.Marshal(src)
	require.NoError(t, err)
	var expected = `{"bool":{"filter":[{"range":{"@timestamp":{"from":"2020-04-06T12:55:00","include_lower":true,"include_upper":true,"to":null}}},{"match_phrase":{"level":{"query":"info"}}}]}}`
	require.Equal(t, expected, string(b), string(b))
}
