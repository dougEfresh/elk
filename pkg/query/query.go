/*
Copyright © 2020 Douglas Chimento <dchimento@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package query

import (
	"context"
	"fmt"

	"github.com/olivere/elastic/v7"
	"gitlab.com/dougEfresh/elk/v7/pkg/config"
)

type Hit struct {
	Hit *elastic.SearchHit
	Err error
}

type Searcher interface {
	Search(ctx context.Context, queryDef *config.QueryDefinition, hits chan<- *Hit) error
}

type query struct {
	client *elastic.Client
}

func (c *query) Search(ctx context.Context, queryDef *config.QueryDefinition, hits chan<- *Hit) error {
	q, err := convertQuery(queryDef)
	if err != nil {
		return err
	}
	svc := c.client.Scroll(queryDef.Index).Query(q).Sort(queryDef.TimestampField, true).Size(queryDef.BatchSize)
	go func() {
		defer func() {
			_ = svc.Clear(ctx)
		}()
		for {
			res, err := svc.Do(ctx)
			if err != nil {
				hits <- &Hit{Err: err}
				break
			}
			for _, hit := range res.Hits.Hits {
				hits <- &Hit{Err: nil, Hit: hit}
			}
		}
	}()
	return nil
}

func convertQuery(queryDef *config.QueryDefinition) (*elastic.BoolQuery, error) {
	if queryDef.From.IsZero() {
		return nil, fmt.Errorf("'since' time is not defined %s", queryDef)
	}
	q := elastic.NewBoolQuery()
	filters := []elastic.Query{elastic.NewRangeQuery(queryDef.TimestampField).
		IncludeLower(true).From(queryDef.From.Format(config.EsTimeFormat))}
	for n, v := range queryDef.Terms {
		filters = append(filters, elastic.NewMatchPhraseQuery(n, v))
	}
	return q.Filter(filters...), nil
}

func New(config *config.EsConfig) (Searcher, error) {
	ct := &query{}
	var err error
	ct.client, err = elastic.NewClientFromConfig(config.ToClientConfig())
	return ct, err
}
