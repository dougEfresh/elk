package config

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFormat_Merge(t *testing.T) {
	a := defaultFormat()
	same := a.Merge(nil)
	require.Equal(t, *a, *same)

	b := &Format{
		Fields:           []string{"msg"},
		Delimiter:        " ",
		ParseSourceField: "source",
	}

	c := a.Merge(b)

	require.Equal(t, " ", c.Delimiter)
	require.Equal(t, []string{"msg"}, c.Fields)
	require.Equal(t, "source", c.ParseSourceField)

	require.Equal(t, "\t", a.Delimiter)
	require.Equal(t, "", a.ParseSourceField)
	require.Equal(t, []string{"@timestamp", "message"}, a.Fields)

	require.JSONEq(t, `{"Fields":["msg"],"Delimiter":" ","ParseSourceField":"source"}`, b.String(), b.String())
}
