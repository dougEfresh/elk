/*
Copyright © 2020 Douglas Chimento <dchimento@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package config

import (
	"encoding/json"

	clientConf "github.com/olivere/elastic/v7/config"
)

// from https://github.com/olivere/elastic/blob/release-branch.v7/config/config.go
type EsConfig struct {
	URL         string `hcl:"url"`
	Username    string `hcl:"username"`
	Password    string `hcl:"password"`
	Shards      *int   `hcl:"shards"`
	Replicas    *int   `hcl:"replicas"`
	Sniff       *bool  `hcl:"sniff"`
	Healthcheck *bool  `hcl:"healthcheck"`
	Infolog     string `hcl:"info-log"`
	Errorlog    string `hcl:"error-log"`
	Tracelog    string `hcl:"trace-log"`
}

func (e *EsConfig) String() string {
	b, _ := json.MarshalIndent(e, "", "   ")
	return string(b)
}

func defaultEsConfig() *EsConfig {
	shards := 1
	replicates := 0
	return &EsConfig{
		URL:         "http://localhost:9200",
		Username:    "",
		Password:    "",
		Shards:      &shards,
		Replicas:    &replicates,
		Sniff:       nil,
		Healthcheck: nil,
		Infolog:     "",
		Errorlog:    "",
		Tracelog:    "",
	}
}

func (e *EsConfig) Merge(b *EsConfig) *EsConfig {
	result := *e
	if b == nil {
		return &result
	}
	if b.URL != "" {
		result.URL = b.URL
	}
	if b.Username != "" {
		result.Username = b.Username
	}
	if b.Password != "" {
		result.Password = b.Password
	}
	if b.Sniff != nil {
		result.Sniff = b.Sniff
	}
	if b.Healthcheck != nil {
		result.Healthcheck = b.Healthcheck
	}
	if b.Shards != nil {
		result.Shards = b.Shards
	}
	if b.Replicas != nil {
		result.Replicas = b.Replicas
	}
	if b.Infolog != "" {
		result.Infolog = b.Infolog
	}
	if b.Errorlog != "" {
		result.Errorlog = b.Errorlog
	}
	if b.Tracelog != "" {
		result.Tracelog = b.Tracelog
	}
	return &result
}

func (e *EsConfig) ToClientConfig() *clientConf.Config {
	cfg := &clientConf.Config{}
	cfg.URL = e.URL
	cfg.Username = e.Username
	cfg.Password = e.Password
	if e.Replicas != nil {
		cfg.Replicas = *e.Replicas
	}
	if e.Shards != nil {
		cfg.Shards = *e.Shards
	}
	cfg.Tracelog = e.Tracelog
	cfg.Infolog = e.Infolog
	cfg.Errorlog = e.Errorlog
	cfg.Sniff = e.Sniff
	cfg.Healthcheck = e.Healthcheck
	return cfg
}
