/*
Copyright © 2020 Douglas Chimento <dchimento@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package config

import (
	"bytes"
	"os"
	"path/filepath"

	"github.com/hashicorp/hcl"
	"github.com/pkg/errors"
)

func LoadLocalConfig() (*Config, error) {
	return ParseConfig(LocalConfigLocation)
}

func ParseConfig(path string) (*Config, error) {
	// slurp
	var buf bytes.Buffer
	path, err := filepath.Abs(path)
	if err != nil {
		return nil, err
	}
	st, err := os.Stat(path)
	if err != nil {
		return nil, errors.Wrapf(err, "%s does not exits", path)
	}

	var files []string

	if st.IsDir() {
		if files, err = filepath.Glob(path + "/*.hcl"); err != nil {
			return nil, err
		}
	} else {
		files = append(files, path)
	}
	for _, file := range files {
		f, err := os.Open(file)
		if err != nil {
			return nil, err
		}
		if _, err = buf.ReadFrom(f); err != nil {
			_ = f.Close()
			return nil, err
		}
		_ = f.Close()
	}
	return parse(buf)
}

func parse(buf bytes.Buffer) (*Config, error) {
	globalConfig := &Config{}
	err := hcl.Decode(globalConfig, buf.String())
	if err != nil {
		return nil, err
	}
	profiles := map[string]*Profile{}
	for _, profile := range globalConfig.Profiles {
		profiles[profile.Name] = profile
	}
	for i, command := range globalConfig.Commands {
		profileCmd := mergeCmdProfiles(profiles, &SubCommand{
			QueryDefinition: command.QueryDefinition,
			Format:          command.Format,
			EsConfig:        command.EsConfig,
			ProfileHcl:      command.ProfileHcl,
			Name:            command.Name,
		})

		if profileCmd.EsConfig != nil {
			command.EsConfig = profileCmd.EsConfig.Merge(command.EsConfig)
		}
		if profileCmd.QueryDefinition != nil {
			command.QueryDefinition = profileCmd.QueryDefinition.Merge(command.QueryDefinition)
		}
		if profileCmd.Format != nil {
			command.Format = profileCmd.Format.Merge(command.Format)
		}
		if globalConfig.EsConfig != nil {
			command.EsConfig = globalConfig.EsConfig.Merge(command.EsConfig)
		}
		if globalConfig.Format != nil {
			command.Format = globalConfig.Format.Merge(command.Format)
		}
		if globalConfig.QueryDefinition != nil {
			command.QueryDefinition = globalConfig.QueryDefinition.Merge(command.QueryDefinition)
		}
		for j, subCmd := range command.SubCommands {
			subCmd = command.MergeSub(subCmd)
			subCmd = mergeCmdProfiles(profiles, subCmd)

			//result.EsConfig = command.EsConfig.Merge(result.EsConfig)
			//
			//result.QueryDefinition = command.QueryDefinition.Merge(result.QueryDefinition)
			//result.Format = command.Format.Merge(result.Format)
			command.SubCommands[j] = subCmd
		}
		globalConfig.Commands[i] = command
	}
	return globalConfig, nil
}

func mergeCmdProfiles(profiles map[string]*Profile, c *SubCommand) *SubCommand {
	result := *c
	for _, name := range c.ProfileHcl {
		p, ok := profiles[name]
		if !ok {
			continue
		}
		if result.QueryDefinition != nil {
			result.QueryDefinition = result.QueryDefinition.Merge(p.QueryDefinition)
		} else {
			result.QueryDefinition = p.QueryDefinition
		}
		if result.EsConfig != nil {
			result.EsConfig = result.EsConfig.Merge(p.EsConfig)
		} else {
			result.EsConfig = p.EsConfig
		}
		if result.Format != nil {
			result.Format = result.Format.Merge(p.Format)
		} else {
			result.Format = p.Format
		}
	}
	return &result
}
