// Copyright © 2020.  Douglas Chimento <dchimento@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package config

import (
	"time"
)

func defaultSince() string {
	return time.Now().UTC().Add(15 * time.Minute * -1).Format(EsTimeFormat)
}

func DefaultConfig() *Config {
	queryDef := defaultQueryDefinition()
	es := defaultEsConfig()
	cfg := &Config{
		EsConfig:        es,
		Commands:        []*Command{},
		QueryDefinition: queryDef,
		Format:          defaultFormat(),
		Verbose:         false,
	}
	return cfg
}

type Config struct {
	EsConfig        *EsConfig        `hcl:"es"`
	Format          *Format          `hcl:"format"`
	Commands        []*Command       `hcl:"command"`
	QueryDefinition *QueryDefinition `hcl:"query" json:"-"`
	Verbose         bool             `json:"-"`
	DryRun          bool
	Profiles        []*Profile `hcl:"profile"`
	// ExtraKeysHCL is used by hcl to surface unexpected keys
	ExtraKeysHCL []string `hcl:",unusedKeys" json:"-"`
}

// Merge merges two configurations.
func (c *Config) Merge(b *Config) *Config {
	result := *c
	if b == nil {
		return &result
	}
	if c.EsConfig == nil {
		result.EsConfig = b.EsConfig
	} else {
		result.EsConfig = c.EsConfig.Merge(b.EsConfig)
	}

	if c.QueryDefinition == nil {
		result.QueryDefinition = b.QueryDefinition
	} else {
		result.QueryDefinition = c.QueryDefinition.Merge(b.QueryDefinition)
	}

	if c.Format == nil {
		result.Format = b.Format
	} else {
		result.Format = c.Format.Merge(b.Format)
	}
	for _, command := range b.Commands {
		resultCmd := *command
		if result.QueryDefinition != nil {
			resultCmd.QueryDefinition = result.QueryDefinition.Merge(resultCmd.QueryDefinition)
		}
		if result.EsConfig != nil {
			resultCmd.EsConfig = result.EsConfig.Merge(resultCmd.EsConfig)
		}
		if result.Format != nil {
			resultCmd.Format = result.Format.Merge(resultCmd.Format)
		}
		for i, subCommand := range resultCmd.SubCommands {
			resultCmd.SubCommands[i] = resultCmd.MergeSub(subCommand)
		}
		result.Commands = append(result.Commands, &resultCmd)
	}
	return &result
}
