// Copyright © 2020.  Douglas Chimento <dchimento@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package config

import "encoding/json"

type Format struct {
	Fields           []string `hcl:"fields"`
	Delimiter        string   `hcl:"delimiter"`
	ParseSourceField string   `hcl:"source"` // use this field to parse/format messages
}

func (f *Format) String() string {
	b, _ := json.MarshalIndent(f, "", "  ")
	return string(b)
}

func (f *Format) Merge(b *Format) *Format {
	result := *f
	if b == nil {
		return &result
	}
	if len(b.Fields) > 0 {
		result.Fields = b.Fields
	}
	if b.Delimiter != "" {
		result.Delimiter = b.Delimiter
	}
	if b.ParseSourceField != "" {
		result.ParseSourceField = b.ParseSourceField
	}
	return &result
}

func defaultFormat() *Format {
	return &Format{
		Fields:           []string{"@timestamp", "message"},
		Delimiter:        "\t",
		ParseSourceField: "",
	}
}
