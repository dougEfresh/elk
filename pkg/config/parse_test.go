package config

import (
	"bytes"
	"errors"
	"io"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLoadLocalConfig(t *testing.T) {
	_, err := LoadLocalConfig()
	var pathError *os.PathError
	if errors.As(err, &pathError) {
		return
	}
	require.NoError(t, err)
}

// nolint:funlen
func TestParse(t *testing.T) {
	cfg, err := ParseConfig("test.hcl")
	require.NoError(t, err)
	require.NotNil(t, cfg.EsConfig)
	require.Equal(t, "url", cfg.EsConfig.URL)
	require.Equal(t, "user", cfg.EsConfig.Username)
	require.Equal(t, "pass", cfg.EsConfig.Password)
	require.Equal(t, 1, *cfg.EsConfig.Shards)
	require.Equal(t, 2, *cfg.EsConfig.Replicas)
	require.Equal(t, true, *cfg.EsConfig.Healthcheck)
	require.Equal(t, true, *cfg.EsConfig.Sniff)
	require.Equal(t, "info.log", cfg.EsConfig.Infolog)
	require.Equal(t, "trace.log", cfg.EsConfig.Tracelog)
	require.Equal(t, "error.log", cfg.EsConfig.Errorlog)
	require.Equal(t, 2, len(cfg.Profiles))
	require.Equal(t, "myservice", cfg.Profiles[0].Name)
	require.Equal(t, "errors", cfg.Profiles[1].Name)

	// Format
	require.Equal(t, []string{"time", "level", "message"}, cfg.Format.Fields)
	require.Equal(t, "\t", cfg.Format.Delimiter)
	require.Equal(t, "", cfg.Format.ParseSourceField)

	//QueryDef
	require.NotNil(t, cfg.QueryDefinition)
	require.Equal(t, "index*", cfg.QueryDefinition.Index)
	require.Equal(t, "@timestamp", cfg.QueryDefinition.TimestampField)
	require.Equal(t, 1, len(cfg.QueryDefinition.Terms))
	require.Equal(t, map[string]string{"level": "info"}, cfg.QueryDefinition.Terms)

	require.Equal(t, 2, len(cfg.Commands))
	cmd := cfg.Commands[0] // webserver command
	require.Equal(t, "webserver", cmd.Name)
	require.Equal(t, "", cmd.Description)
	require.Equal(t, "webserver*", cmd.QueryDefinition.Index)
	require.Equal(t, "@timestamp", cmd.QueryDefinition.TimestampField)

	require.Equal(t, 1, len(cmd.SubCommands))
	subcmd := cmd.SubCommands[0]
	require.Equal(t, "webserver-errors", subcmd.Name)
	require.Equal(t, 1, len(subcmd.QueryDefinition.Terms))
	require.Equal(t, map[string]string{"level": "error"}, subcmd.QueryDefinition.Terms)
	require.NotNil(t, subcmd.Format)

	// service command
	cmd = cfg.Commands[1]
	require.Equal(t, 1, len(cmd.SubCommands))
	require.Equal(t, "service", cmd.Name)
	require.Equal(t, "", cmd.Description)
	//require.Equal(t, []string{"myservice"}, cmd.ProfileHcl)
	require.Equal(t, "http://elastic", cmd.EsConfig.URL)
	require.Equal(t, "service*", cmd.QueryDefinition.Index)
	require.Equal(t, 1, len(cmd.SubCommands))

	// service-errors command
	subcmd = cmd.SubCommands[0]
	require.Equal(t, "service-errors", subcmd.Name)
	require.Equal(t, "service*", subcmd.QueryDefinition.Index)

	require.NotNil(t, subcmd.Format)
	require.Equal(t, 1, len(subcmd.Format.Fields))
	require.Equal(t, []string{"message"}, subcmd.Format.Fields)
	require.Equal(t, "\t", subcmd.Format.Delimiter)
	require.Equal(t, 1, len(subcmd.QueryDefinition.Terms))
	require.Equal(t, "yesterday", subcmd.QueryDefinition.FromHCL)

	require.Equal(t, map[string]string{"level": "error"}, subcmd.QueryDefinition.Terms)
}

func TestParseFile(t *testing.T) {
	tempDir := os.TempDir()
	fName := filepath.Join(tempDir, t.Name())
	_ = os.RemoveAll(fName) // remove any old file
	f, err := os.OpenFile(fName, os.O_WRONLY|os.O_CREATE, 0755)
	require.NoError(t, err)
	defer func() {
		_ = f.Close()
		_ = os.Remove(fName)
	}()

	buf := bytes.NewBufferString("")
	_, err = io.Copy(f, buf)
	require.NoError(t, err)
	err = f.Close()
	require.NoError(t, err)
	_, err = ParseConfig(fName)
	require.NoError(t, err)
}

func TestBadFile(t *testing.T) {
	_, err := ParseConfig("/i/should/never/exists.txt")
	require.Error(t, err)
}
