// Copyright © 2020.  Douglas Chimento <dchimento@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package config

type Command struct {
	Name            string           `hcl:",key"`
	Description     string           `hcl:"description"`
	QueryDefinition *QueryDefinition `hcl:"query"`
	Format          *Format          `hcl:"format"`
	EsConfig        *EsConfig        `hcl:"es"`
	ProfileHcl      []string         `hcl:"profiles"`
	ExtraKeysHCL    []string         `hcl:",unusedKeys" json:"-"`
	SubCommands     []*SubCommand    `hcl:"subcommand"`
}

type SubCommand struct {
	Name            string           `hcl:",key"`
	Description     string           `hcl:"description"`
	QueryDefinition *QueryDefinition `hcl:"query"`
	Format          *Format          `hcl:"format"`
	EsConfig        *EsConfig        `hcl:"es"`
	ProfileHcl      []string         `hcl:"profiles"`
	ExtraKeysHCL    []string         `hcl:",unusedKeys" json:"-"`
}

func (c *Command) MergeSub(b *SubCommand) *SubCommand {
	result := &SubCommand{
		Name:            c.Name,
		Description:     c.Description,
		QueryDefinition: c.QueryDefinition,
		Format:          c.Format,
		ProfileHcl:      c.ProfileHcl,
	}
	if b == nil {
		return result
	}
	if c.QueryDefinition != nil {
		result.QueryDefinition = c.QueryDefinition.Merge(b.QueryDefinition)
	}
	result.ProfileHcl = b.ProfileHcl
	//	if len(b.ProfileHcl) > 0 {
	//		result.ProfileHcl = append(result.ProfileHcl, b.ProfileHcl...)
	//	}
	if b.Name != "" {
		result.Name = b.Name
	}
	if b.Description != "" {
		result.Description = b.Description
	}
	if c.Format != nil {
		result.Format = c.Format.Merge(b.Format)
	}
	return result
}
