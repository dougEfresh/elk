// +build !windows,!darwin

/*
Copyright © 2020 Douglas Chimento <dchimento@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package config

import (
	"os"
	"path/filepath"
)

// User wide configuration folders:
//
//   - Windows: %APPDATA% (C:\Users\<User>\AppData\Roaming)
//   - Linux/BSDs: ${XDG_CONFIG_HOME} (${HOME}/.config)
//   - MacOSX: "${HOME}/Library/Application Support"
// https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

var LocalConfigLocation string // nolint:gochecknoglobals

// nolint:gochecknoinits
func init() {
	// nolint:gocritic
	if os.Getenv("ELK_CONFIG") != "" {
		LocalConfigLocation = os.Getenv("ELK_CONFIG")
	} else if os.Getenv("XDG_CONFIG_HOME") != "" {
		LocalConfigLocation = filepath.Join(os.Getenv("XDG_CONFIG_HOME"), "elk")
	} else {
		LocalConfigLocation = filepath.Join(os.Getenv("HOME"), ".config", "elk")
	}
}
