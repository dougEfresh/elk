package config

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestQueryDefinition_Merge(t *testing.T) {
	a := defaultQueryDefinition()
	terms := map[string]string{
		"level": "info",
	}
	b := &QueryDefinition{
		Index:          "index",
		Terms:          terms,
		TimestampField: "@time",
		FromHCL:        "",
		BatchSize:      10001,
	}
	same := a.Merge(nil)
	c := a.Merge(b)

	require.Equal(t, *a, *same)
	require.Equal(t, "index", c.Index)
	require.Equal(t, a.FromHCL, c.FromHCL)
	require.Equal(t, "@time", c.TimestampField)
	require.Equal(t, 10001, c.BatchSize)
	require.Equal(t, 1, len(c.Terms))
	require.Equal(t, terms, c.Terms)

	b.FromHCL = "today"
	c = a.Merge(b)
	require.Equal(t, "today", c.FromHCL)
	require.Equal(t, 1, len(c.Terms))
	require.Equal(t, terms, c.Terms)
	require.NotEqual(t, "", c.String())
}

func TestQueryDefinition_Default(t *testing.T) {
	def := defaultQueryDefinition()
	defSince, err := ParseFrom("") //
	require.NoError(t, err)
	require.NotNil(t, defSince)
	expected := QueryDefinition{
		Index:          "",
		Terms:          nil,
		TimestampField: "@timestamp",
		FromHCL:        defSince.Format(EsTimeFormat),
		From:           defSince,
		BatchSize:      1000,
	}
	require.Equal(t, expected, *def)
}

func TestQueryDefinition_FormatAfterDate(t *testing.T) {
	after, err := time.Parse(EsTimeFormat, defaultSince())
	require.NoError(t, err)
	def := &QueryDefinition{FromHCL: after.Format(EsTimeFormat)}
	formatted, err := ParseFrom(def.FromHCL)
	require.NoError(t, err)
	require.Equal(t, after, formatted)

	def.FromHCL = "yesterday"
	formatted, err = ParseFrom(def.FromHCL)
	require.NoError(t, err)
	require.NotEqual(t, "", formatted)

	def.FromHCL = "today"
	formatted, err = ParseFrom(def.FromHCL)
	require.NoError(t, err)
	require.NotEqual(t, "", formatted)
}
