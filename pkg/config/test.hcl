es {
  url = "url"
  username = "user"
  password = "pass"
  shards = 1
  replicas = 2
  healthcheck = true
  info-log = "info.log"
  error-log = "error.log"
  trace-log = "trace.log"
  sniff = true
}

query {
  index = "index*"
  terms {
    level = "info"
  }
  timestamp = "@timestamp"
}

format {
  fields = [
    "time",
    "level",
    "message"]
  delimiter = "\t"
}

profile "myservice" {
  es {
    url = "http://elastic"
  }
  query {
    index = "service*"
  }
}

profile "errors" {
  query {
    terms {
      level = "error"
    }
    from = "yesterday"
  }
  format {
    fields = ["message"]
  }
}

command webserver {
  query {
    index = "webserver*"
  }
  subcommand "webserver-errors" {
    query {
      terms {
        level = "error"
      }
    }
  }
}

command service {
  //profiles = ["myservice"]
  es {
    url = "http://elastic"
  }
  query {
    index = "service*"
  }
  subcommand "service-errors" {
    profiles = ["errors"]
  }
}