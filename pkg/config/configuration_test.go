package config

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestConfig_Default(t *testing.T) {
	c := DefaultConfig()
	es := defaultEsConfig()
	def := defaultQueryDefinition()
	f := defaultFormat()
	require.Equal(t, *es, *c.EsConfig)
	require.Equal(t, *def, *c.QueryDefinition)
	require.Equal(t, false, c.Verbose)
	require.Equal(t, 0, len(c.Commands))
	require.Equal(t, *f, *c.Format)
}

// nolint:funlen
func TestConfig_Merge(t *testing.T) {
	a := DefaultConfig()
	defEsConfig := defaultEsConfig()
	errTerms := map[string]string{
		"level": "error",
	}
	infoTerms := map[string]string{
		"level": "info",
	}
	subCmd := &SubCommand{
		Name:        "subcmd",
		Description: "sub command",
		QueryDefinition: &QueryDefinition{
			Terms: errTerms,
		},
	}
	cmd := &Command{
		Name:        "name",
		Description: "description",
		QueryDefinition: &QueryDefinition{
			Index: "webserver*",
		},
		Format: &Format{
			Fields: []string{"time", "msg"},
		},
		SubCommands: []*SubCommand{subCmd},
	}

	b := &Config{
		EsConfig: defEsConfig,
		Commands: []*Command{cmd},
		QueryDefinition: &QueryDefinition{
			Index: "index*",
			Terms: infoTerms,
		},
	}
	b.QueryDefinition.FromHCL = a.QueryDefinition.FromHCL
	same := a.Merge(nil)
	require.Equal(t, *a, *same)

	c := a.Merge(b)
	require.Equal(t, "index*", c.QueryDefinition.Index)
	require.Equal(t, 1, len(c.QueryDefinition.Terms))
	require.Equal(t, infoTerms, c.QueryDefinition.Terms)
	require.Equal(t, *defEsConfig, *a.EsConfig)

	require.Equal(t, 1, len(c.Commands))

	cmd = c.Commands[0]
	require.Equal(t, "name", cmd.Name)
	require.Equal(t, "description", cmd.Description)
	require.Equal(t, "webserver*", cmd.QueryDefinition.Index)
	require.Equal(t, []string{"time", "msg"}, cmd.Format.Fields)
	require.Equal(t, 1, len(cmd.QueryDefinition.Terms))
	require.Equal(t, infoTerms, cmd.QueryDefinition.Terms)
	require.Equal(t, a.QueryDefinition.FromHCL, cmd.QueryDefinition.FromHCL)
	require.Equal(t, *defEsConfig, *cmd.EsConfig)

	require.Equal(t, 1, len(cmd.SubCommands))
	subCmd = cmd.SubCommands[0]
	require.Equal(t, "subcmd", subCmd.Name)
	require.Equal(t, "sub command", subCmd.Description)
	require.Equal(t, "webserver*", subCmd.QueryDefinition.Index)
	require.Equal(t, 1, len(subCmd.QueryDefinition.Terms))
	require.Equal(t, errTerms, subCmd.QueryDefinition.Terms)
	require.Equal(t, []string{"time", "msg"}, subCmd.Format.Fields)
	require.Equal(t, a.QueryDefinition.FromHCL, subCmd.QueryDefinition.FromHCL)
	require.Equal(t, *c.EsConfig, *b.EsConfig)
}
