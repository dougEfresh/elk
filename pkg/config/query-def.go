// Copyright © 2020.  Douglas Chimento <dchimento@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package config

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/olebedev/when"
	"github.com/olebedev/when/rules/common"
	"github.com/olebedev/when/rules/en"
)

// ESTimeFormat - format for elastic search
const EsTimeFormat = "2006-01-02T15:04:05"
const defaultBatchSize = 1000

var w = when.New(nil) // nolint:gochecknoglobals

type QueryDefinition struct {
	Index          string            `hcl:"index"`
	Terms          map[string]string `hcl:"terms"`
	TimestampField string            `hcl:"timestamp"`
	FromHCL        string            `hcl:"from"`
	From           time.Time         `hcl:"-"`
	BatchSize      int               `hcl:"batch"`
}

func (q *QueryDefinition) String() string {
	b, _ := json.MarshalIndent(q, "", "   ")
	return string(b)
}

func defaultQueryDefinition() *QueryDefinition {
	t, _ := ParseFrom("") // default 15 min ago
	return &QueryDefinition{
		TimestampField: "@timestamp",
		FromHCL:        t.Format(EsTimeFormat),
		From:           t,
		BatchSize:      defaultBatchSize,
	}
}

// nolint:goconst
func ParseFrom(since string) (time.Time, error) {
	if since == "" {
		since = defaultSince()
	}
	t, err := time.Parse(EsTimeFormat, since)
	if err == nil {
		return t, nil
	}
	t = t.UTC()
	now := time.Now().UTC()
	if since == "today" {
		now = now.Round(time.Hour)
		now = now.Add(time.Duration(now.Hour()) * time.Hour * -1)
		return now, nil
	}
	r, err := w.Parse(since, now)
	if err != nil {
		return time.Time{}, err
	}
	if r == nil {
		return time.Time{}, fmt.Errorf("could not parse %s into a time", since)
	}
	return r.Time, nil
}

func (q *QueryDefinition) Merge(b *QueryDefinition) *QueryDefinition {
	result := *q
	if b == nil {
		return &result
	}
	if b.Index != "" && b.Index != q.Index {
		result.Index = b.Index
	}
	if b.TimestampField != "" && b.TimestampField != q.TimestampField {
		result.TimestampField = b.TimestampField
	}
	if b.FromHCL != "" && b.FromHCL != q.FromHCL {
		result.FromHCL = b.FromHCL
	}
	result.From, _ = ParseFrom(result.FromHCL)
	if len(b.Terms) > 0 {
		result.Terms = b.Terms
	}
	if b.BatchSize > 0 {
		result.BatchSize = b.BatchSize
	}
	return &result
}

// nolint:gochecknoinits
func init() {
	w.Add(en.All...)
	w.Add(common.All...)
}
