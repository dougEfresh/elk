package config

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestEsConfig_ToClientConfig(t *testing.T) {
	var boolPtr = true
	var shards = 1
	var replicas = 2
	c := &EsConfig{
		URL:         "http://localhost",
		Username:    "username",
		Password:    "password",
		Shards:      &shards,
		Replicas:    &replicas,
		Sniff:       &boolPtr,
		Healthcheck: &boolPtr,
		Infolog:     "info",
		Errorlog:    "error",
		Tracelog:    "trace",
	}
	clientConfig := c.ToClientConfig()
	require.Equal(t, c.URL, clientConfig.URL)
	require.Equal(t, c.Username, clientConfig.Username)
	require.Equal(t, c.Password, clientConfig.Password)
	require.Equal(t, *c.Shards, clientConfig.Shards)
	require.Equal(t, *c.Replicas, clientConfig.Replicas)
	require.Equal(t, *c.Healthcheck, *clientConfig.Healthcheck)
	require.Equal(t, *c.Sniff, *clientConfig.Sniff)
	require.Equal(t, c.Infolog, clientConfig.Infolog)
	require.Equal(t, c.Errorlog, clientConfig.Errorlog)
	require.Equal(t, c.Tracelog, clientConfig.Tracelog)
}

func TestEsConfigMerge(t *testing.T) {
	var boolPtr bool
	var shards int
	var replicas int
	a := defaultEsConfig()
	b := &EsConfig{
		URL:         "http://localhost",
		Username:    "username",
		Password:    "password",
		Shards:      &shards,
		Replicas:    &replicas,
		Sniff:       &boolPtr,
		Healthcheck: &boolPtr,
		Infolog:     "info",
		Errorlog:    "error",
		Tracelog:    "trace",
	}
	same := a.Merge(nil)
	require.Equal(t, *same, *a)
	c := a.Merge(b)
	require.Equal(t, *b, *c)
	require.NotEqual(t, "", b.String())
}
