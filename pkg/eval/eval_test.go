/*
Copyright © 2020 Douglas Chimento <dchimento@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package eval

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/olivere/elastic/v7"
	"github.com/stretchr/testify/require"
	"gitlab.com/dougEfresh/elk/v7/pkg/config"
	"gitlab.com/dougEfresh/elk/v7/pkg/query"
)

// nolint:lll
func TestEvalExpress(t *testing.T) {
	f, err := os.Open("eval_test.json")
	require.NoError(t, err)
	b, err := ioutil.ReadAll(f)
	require.NoError(t, err)
	var data map[string]interface{}
	err = json.Unmarshal(b, &data)
	require.NoError(t, err)
	require.Equal(t, 4, len(data))

	actual, err := evaluateExpression(data, "@timestamp")
	require.NoError(t, err)
	require.Equal(t, "2000-04-06T12:56.00", actual)

	actual, err = evaluateExpression(data, "message")
	require.NoError(t, err)
	require.Equal(t, "hi", actual)

	actual, err = evaluateExpression(data, "level")
	require.NoError(t, err)
	require.Equal(t, "info", actual)

	actual, err = evaluateExpression(data, "complex")
	require.NoError(t, err)
	require.Equal(t, `{"metadata":{"datacenter":"dc1","environment":"prod","host":"webserver"},"tags":["a","b","c"]}`, actual)

	actual, err = evaluateExpression(data, "complex.tags")
	require.NoError(t, err)
	require.Equal(t, `["a","b","c"]`, actual)

	actual, err = evaluateExpression(data, "complex.metadata.datacenter")
	require.NoError(t, err)
	require.Equal(t, `dc1`, actual)

	actual, err = evaluateExpression(data, "complex.metadata.environment")
	require.NoError(t, err)
	require.Equal(t, `prod`, actual)

	actual, err = evaluateExpression(data, "complex.metadata.host")
	require.NoError(t, err)
	require.Equal(t, `webserver`, actual)

	_, err = evaluateExpression(data, "complex.metadata.host.blah")
	require.Error(t, err)
}

func TestMessage(t *testing.T) {
	f, err := os.Open("eval_test.json")
	require.NoError(t, err)
	b, err := ioutil.ReadAll(f)
	require.NoError(t, err)
	var sb strings.Builder
	format := &config.Format{
		Fields:           []string{"@timestamp", "message", "level"},
		Delimiter:        "\t",
		ParseSourceField: "",
	}
	err = appendMessage(b, &sb, format)
	require.NoError(t, err)
	const expected = "2000-04-06T12:56.00\thi\tinfo\n"
	require.Equal(t, expected, sb.String())
}

func TestEval(t *testing.T) {
	f, err := os.Open("eval_test.json")
	require.NoError(t, err)
	b, err := ioutil.ReadAll(f)
	require.NoError(t, err)
	hits := []*elastic.SearchHit{{
		Source: b,
	}}
	var sb strings.Builder
	hitCh := make(chan *query.Hit)
	defer close(hitCh)
	go func() {
		for _, hit := range hits {
			hitCh <- &query.Hit{Hit: hit}
		}
		hitCh <- &query.Hit{Err: io.EOF}
	}()
	format := &config.Format{
		Fields:           []string{"@timestamp", "message", "level"},
		Delimiter:        "\t",
		ParseSourceField: "",
	}
	err = Write(hitCh, &sb, format)
	require.NoError(t, err)
	const expected = "@timestamp\tmessage\tlevel\n2000-04-06T12:56.00\thi\tinfo\n"
	require.Equal(t, expected, sb.String())
}

func TestEvalRaw(t *testing.T) {
	f, err := os.Open("eval_test.json")
	require.NoError(t, err)
	b, err := ioutil.ReadAll(f)
	require.NoError(t, err)
	hits := []*elastic.SearchHit{{
		Source: b,
	}}
	var sb strings.Builder
	hitCh := make(chan *query.Hit)
	defer close(hitCh)
	go func() {
		for _, hit := range hits {
			hitCh <- &query.Hit{Hit: hit}
		}
		hitCh <- &query.Hit{Err: io.EOF}
	}()
	format := &config.Format{
		Fields: []string{"*"},
	}
	err = Write(hitCh, &sb, format)
	require.NoError(t, err)
	b = append(b, byte(10)) // append newline \n
	require.Equal(t, string(b), sb.String())
}
