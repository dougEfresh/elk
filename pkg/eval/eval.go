/*
Copyright © 2020 Douglas Chimento <dchimento@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package eval

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/dougEfresh/elk/v7/pkg/config"
	"gitlab.com/dougEfresh/elk/v7/pkg/query"
)

// evaluateExpression Expression evaluation function. It uses map as a model and evaluates expression given as
// the parameter using dot syntax:
// "foo" evaluates to model[foo]
// "foo.bar" evaluates to model[foo][bar]
// If a key given in the expression does not exist in the model, function will return empty string and
// an elog.Error.
func evaluateExpression(model map[string]interface{}, fieldExpression string) (string, error) {
	if fieldExpression == "" {
		b, err := json.Marshal(model)
		return string(b), err
	}
	parts := strings.SplitN(fieldExpression, ".", 2)
	expression := parts[0]
	value, ok := model[expression]
	if !ok {
		return "", nil
	}
	if len(parts) == 1 {
		s, ok := value.(string)
		if ok {
			return s, nil
		}
		b, err := json.Marshal(value)
		if err != nil {
			return "", errors.Wrapf(err, "failed to marshal %v", value)
		}
		return string(b), nil
	}

	next, ok := value.(map[string]interface{})
	if !ok {
		return "", fmt.Errorf("next value is not a map %T (%v)", value, value)
	}
	return evaluateExpression(next, parts[1])
}

// Message - process a raw message with a delim and fields
// "fields" are the "keys" in the raw json message (map[string]interface{}
// Example:
func appendMessage(message json.RawMessage, w io.Writer, format *config.Format) error {
	if len(format.Fields) == 1 && format.Fields[0] == "*" {
		// show entire message
		if _, err := w.Write(message); err != nil {
			return err
		}
		if _, err := w.Write([]byte("\n")); err != nil {
			return err
		}
		return nil
	}
	var model map[string]interface{}
	if err := json.Unmarshal(message, &model); err != nil {
		return errors.Wrapf(err, "failed to unmarshal %s", string(message))
	}
	for i := 0; i < len(format.Fields); i++ {
		field := format.Fields[i]
		result, err := evaluateExpression(model, field)
		if err != nil {
			return err
		}
		if _, err := w.Write([]byte(result)); err != nil {
			return err
		}
		if i != len(format.Fields)-1 { // don't print delim on last field
			if _, err := w.Write([]byte(format.Delimiter)); err != nil {
				return err
			}
		}
	}
	if _, err := w.Write([]byte("\n")); err != nil {
		return err
	}
	return nil
}

// Write will process dot (".") notation fields and return the string or json value of the fields
// See tests for examples
//    Write(hits, os.StdOut, "\t", "message", env.name, env.datacenter)
func Write(hits <-chan *query.Hit, w io.Writer, format *config.Format) error {
	if len(format.Fields) > 0 && format.Fields[0] != "*" {
		// don't show header if there are no fields defined or it is "*"
		if _, err := w.Write([]byte(strings.Join(format.Fields, format.Delimiter))); err != nil {
			return err
		}
		if _, err := w.Write([]byte("\n")); err != nil {
			return err
		}
	}
	for hit := range hits {
		if hit.Err == io.EOF {
			return nil
		}
		if hit.Err != nil {
			return hit.Err
		}
		if err := appendMessage(hit.Hit.Source, w, format); err != nil {
			return err
		}
	}
	return nil
}
