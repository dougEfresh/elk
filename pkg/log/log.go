/*
Copyright © 2020 Douglas Chimento <dchimento@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package log

import (
	"os"

	logger "github.com/sirupsen/logrus"
)

var Logger *logger.Logger = logger.New() // nolint:gochecknoglobals

// nolint:gochecknoinits
func init() {
	Logger.SetFormatter(&logger.TextFormatter{
		ForceColors:               false,
		DisableColors:             false,
		EnvironmentOverrideColors: false,
		DisableTimestamp:          true,
		FullTimestamp:             false,
		TimestampFormat:           "",
		DisableSorting:            true,
		SortingFunc:               nil,
		DisableLevelTruncation:    false,
		QuoteEmptyFields:          true,
		FieldMap:                  nil,
	})
	Logger.SetLevel(logger.InfoLevel)
	Logger.SetOutput(os.Stderr)
	Logger.SetNoLock()
}
