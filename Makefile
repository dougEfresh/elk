BIN_DIR := .tools/bin

GO := go
ifdef GO_BIN
	GO = $(GO_BIN)
endif

GOLANGCI_LINT_VERSION := 1.29.0
GOLANGCI_LINT := $(BIN_DIR)/golangci-lint_$(GOLANGCI_LINT_VERSION)
GIT_COMMIT := $(shell git rev-parse --short HEAD 2> /dev/null || echo "no-revision")
GIT_COMMIT_MESSAGE := $(shell git show -s --format='%s' 2> /dev/null | tr ' ' _ | tr -d "'")
GIT_TAG := $(shell git describe --tags 2> /dev/null || echo "no-tag")
GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD 2> /dev/null || echo "no-branch")
BUILD_TIME := $(shell date +%FT%T%z)
VERSION_PACKAGE := gitlab.com/dougEfresh/elk/pkg/version
VERBOSE :=

## all: The default target. Build, test, lint
all: test lint

## tidy: go mod tidy
tidy:
	$(GO) mod tidy -v

## fmt: format all go code
fmt:
	gofmt -s -w .
hooks:
	cp -v scripts/pre-commit .git/hooks

build:
	$(GO) build -o elk main.go

build-all:
	$(GO) build ./...

# Use this target when building in CI so that you get all the lovely version information in the resulting executable
ci-build:
	$(GO) build -o elk -v -ldflags '-X $(VERSION_PACKAGE).GitHash=$(GIT_COMMIT) -X $(VERSION_PACKAGE).GitTag=$(GIT_TAG) -X $(VERSION_PACKAGE).GitBranch=$(GIT_BRANCH) -X $(VERSION_PACKAGE).BuildTime=$(BUILD_TIME) ' main.go

## test: Run all tests
test: build-all
	$(GO) test -cover -race $(VERBOSE)  `go list ./...`

## lint: lint all go code
lint: $(GOLANGCI_LINT)
	$(GOLANGCI_LINT) run --fast --enable-all -D wsl -D testpackage -D godot -D goerr113 --disable gofumpt

$(GOLANGCI_LINT):
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(BIN_DIR) v$(GOLANGCI_LINT_VERSION)
	mv $(BIN_DIR)/golangci-lint $(GOLANGCI_LINT)

help:
	@echo "Usage: \n"
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' |  sed -e 's/^/ /'
